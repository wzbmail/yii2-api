# yii2-api

#### 介绍

yii2 api助手，包含api常用配置、响应数据格式、语言等数据处理。
通过该助手可快速实现api搭建、数据处理。

#### 软件架构

#### 安装教程

```
composer require forwzb/yii2-api
```

#### 使用说明

### API 权限控制、跨域、响应格式快捷配置

以下配置为常规API配置，可以根据项目需要配置

```php
//main.php

    'bootstrap' => [
        'log',
        //框架引导时会执行匿名函数
        [
            'class' => ApiPresetBootstrap::class,
            'basicAuth' => [
                'optional' => ['test/*']
            ],
        ]
    ],
        
```

`ApiPresetBootstrap` 作为yii2 bootstrap调用，封装了 corsFilter 跨域、
baseAuth 认证 、contentNegotiator 响应格式和语言 等行为控制，满足日常项目权限控制，可根据项目自行调整

### helper 助手类

```php
ArrayFormatter // 便捷数组转换器，包含客户端常用数据格式转化
```

### api

```php
ApiFormatter // 约定响应体格式的对象  
ApiFormatterTrait //即可使用 `$this->success()` 等快捷方法
```

### model 模型相关

```php
ForbidDeleteBehavior // 禁止删除行为
```

### enum 枚举标签约定

```php
EnumLabelTrait // labels 枚举助手，快捷获取label映射
EnumLabelTrait::labels() //label设置映射组
EnumLabelTrait::getLabel(...$case) //获取labe映射组
$case->getLabel() //获取枚举label值
```

#### 参与贡献

#### 特技

