<?php

namespace Forwzb\Yii2Api\api;

use Yii;

trait ApiControllerTrait
{
    use ApiFormatterTrait;

    /**
     * 当前登录用户
     */
    public function getIdentity(): \yii\web\IdentityInterface|null
    {
        return Yii::$app->user->identity;
    }

    public function actionAttribute($name = null)
    {
    }
}