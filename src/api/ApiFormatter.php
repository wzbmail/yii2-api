<?php

namespace Forwzb\Yii2Api\api;

use yii\base\BaseObject;

/**
 * API 响应体数据格式类
 */
class ApiFormatter extends BaseObject
{
    const DEFAULT_CODE = 0;

    public string $message = '';
    public int $code = self::DEFAULT_CODE;
    public $data = null;
    public bool $success = true;

    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;
        return $this;
    }

    public function setData($data): self
    {
        $this->data = $data;
        return $this;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;
        return $this;
    }

    public function success($message = '', $data = null, $code = self::DEFAULT_CODE): self
    {
        return $this
            ->setMessage($message)
            ->setData($data)
            ->setCode($code)
            ->setSuccess(true);
    }

    public function error($message = '', $data = null, $code = self::DEFAULT_CODE): self
    {
        return $this
            ->setMessage($message)
            ->setData($data)
            ->setCode($code)
            ->setSuccess(false);
    }
}