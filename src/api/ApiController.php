<?php

namespace Forwzb\Yii2Api\api;

use Yii;

/**
 * 常规控制器
 * 如何实现
 */
class ApiController extends \yii\rest\Controller
{
    use ApiControllerTrait;

    public function behaviors()
    {
        //开启了全局配置
        $behaviors = parent::behaviors();
        unset($behaviors['contentNegotiator']);
        unset($behaviors['authenticator']);
        //unset($behaviors['rateLimiter']);
        return $behaviors;
    }
}